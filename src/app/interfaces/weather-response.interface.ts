/**
 * API Response for City Data
 */
export interface WeatherResponse {
    /** count */
    cnt: number;
    /** city list */
    list: [{
        /** city id */
        id: number;
        /** city name */
        name: string;
        /** sys */
        sys: {
            /** country name */
            country: string;
            /** sunrise time */
            sunrise: number;
            /** sunset time */
            sunset: number;
        },
        /** main */
        main: {
            /** temperature */
            temp: number;
            /** temp minimum */
            temp_min: number;
            /** temp maximum */
            temp_max: number;
        }
    }]
}
/**
 * Weather forecast of selected city
 */
export interface WeatherForCastResponse {
    /** City Data */
    city: {
        /** city id */
        id: number;
        /** city name */
        name: string;
        /** country name */
        country: string;
    },
    /** city list */
    list: [
        {
            /** date */
            dt: number;
            /** main objects */
            main: {
                /** temperature */
                temp: number;
                /** sea level */
                sea_level: number;
            },
            /** weather */
            weather: [
                {
                    /** description */
                    description: string;
                }
            ],
            /** date text */
            dt_txt: string;
        }
    ]
}

  