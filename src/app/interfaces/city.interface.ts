/**
 * Interface for City Data
 */
export interface City {
    /** City ID */
    id: number;
    /** City Name */
    name: string;
    /** City Temperature */
    temp: number;
    /** Sunrise Time */
    sunrise: string;
    /** Sunset Time */
    sunset: string
}
