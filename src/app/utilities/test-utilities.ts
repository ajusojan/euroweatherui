import { ActivatedRoute } from '@angular/router';

/** fake class for tests */
export const fakeActivatedRoute = {
  snapshot: { data: {} }
} as ActivatedRoute;
