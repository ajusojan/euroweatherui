 /** City Data */
 export class CityData {
   /** Constructor method */
    constructor(
      /** city id */
      public id: number,
      /** city name */
      public name: string,
      /** country */
      public country: String,
      /** temperature */
      public temp: number,
      /** sunrise */
      public sunrise: number,
      /** sunset */
      public sunset: number
    ) { }
  }
  
  /** City ForeCast */
  export class CityForCast {
    /** Constructor method */
    constructor(
      /** date */
      public dt: number,
      /** temperature */
      public temp: number,
      /** sea level */
      public sealevel: number,
      /** date type */
      public date: Date,
    ) { }
  }