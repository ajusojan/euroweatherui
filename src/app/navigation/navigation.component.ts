import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
/**
 * Navigation Component Definition
 */
export class NavigationComponent implements OnInit {

  /**
   * Constructor Method
   * @param router 
   */
  constructor(private router: Router) { }

  /**
   * on init hook
   */
  ngOnInit(): void {
  }
}
