import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-splash-screen',
  templateUrl: './splash-screen.component.html',
  styleUrls: ['./splash-screen.component.scss']
})
/**
 * Splash Screen Component Definition
 */
export class SplashscreenComponent implements OnInit {

  /** splashscreen boolean for hiding splashscreen */
  public showSplashScreen = true;

  /**
   * Constructor Method
   */
  constructor() { }

  /** on init hook */
  ngOnInit(): void {

    setTimeout(() => {
      this.showSplashScreen = !this.showSplashScreen;
    }, 3000);

  }

}
