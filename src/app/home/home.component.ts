import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { firstValueFrom } from 'rxjs';

import { WeatherService } from '../services';
import { CityData } from '../utilities';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
/**
 * Home Component Class Definition
 */
export class HomeComponent implements OnInit {
  /** mat-table columns */
  public displayedColumns = ['name', 'country', 'temperature', 'sunrise', 'sunset'];
  /** mat-table data source */
  public dataSource = new MatTableDataSource<CityData>();

  /**
   * Constructor Method
   * @param weatherService 
   */
  constructor(private weatherService: WeatherService) { }

  /**
   * on init hook
   */
  ngOnInit(): void {
    this.getCityData();
  }

  /**
   * getting data of favourite cities
   */
  private getCityData = async () => {
    try {
      const favouriteCityList = await firstValueFrom(this.weatherService.getFavouriteCities());

      this.dataSource.data = favouriteCityList.list.map((item) => {
        return new CityData(item.id, item.name, item.sys.country, item.main.temp, item.sys.sunrise, item.sys.sunset);
      });
    } catch {
      alert('Something went wrong. Pls try again..!');
    }
  }
}
