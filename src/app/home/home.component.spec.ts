import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { of } from 'rxjs';

import { WeatherService } from '../services';
import { CityData } from '../utilities';
import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HomeComponent],
      imports: [HttpClientModule],
      providers: [WeatherService]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('it should call weather service', () => {
    const spyFavCity = spyOn((component as any).weatherService, 'getFavouriteCities');
    component.ngOnInit();
    expect(spyFavCity).toHaveBeenCalled();
  });

  it('it should update mat table data', fakeAsync(() => {
    debugger;
    spyOn((component as any).weatherService, 'getFavouriteCities').and.returnValue(
      of({ list: [{ id: 123, name: 'ams', sys: { country: 'NL', sunrise: 23232, sunset: 3333 }, main: { temp: 323 } }] })
    );
    component.ngOnInit();
    tick();
    expect(component.dataSource.data[0] as object).toEqual(new CityData(123, 'ams', 'NL', 323, 23232, 3333)
    );
  }));
});
