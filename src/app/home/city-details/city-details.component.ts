import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { firstValueFrom } from 'rxjs';

import { WeatherService } from 'src/app/services';
import { CityForCast } from 'src/app/utilities';

@Component({
  selector: 'app-city-details',
  templateUrl: './city-details.component.html',
  styleUrls: ['./city-details.component.scss']
})
/**
 * City Details Component Definition
 */
export class CityDetailsComponent implements OnInit {

  /** city id */
  private id: string = '';
  /** loader boolean */
  public loaderVisibility = true;
  /** City Forecast data */
  public cityData: CityForCast[] | null = null;
  /** selected city Name */
  public selectedCity: string = '';

  /**
   * Constructor Method
   * @param route ActivatedRoute
   * @param weatherService WeatherService
   */
  constructor(private route: ActivatedRoute, private weatherService: WeatherService) {
    this.id = this.route.snapshot.paramMap.get('id') || '';
  }

  /**
   * on init hook
   */
  ngOnInit(): void {
    this.getCityDetails();
  }

  /**
   * get forecast of selected city
   */
  private async getCityDetails() {
    try {
      this.loaderVisibility = true;
      const cityForeCastResponse = await firstValueFrom(this.weatherService.getCityForCast(this.id?.toString()));
      this.selectedCity = cityForeCastResponse?.city?.name;
      const cityForCastData = cityForeCastResponse?.list.map((item) => {
        return new CityForCast(item?.dt, item?.main?.temp, item?.main?.sea_level, new Date(item?.dt_txt));
      });

      this.cityData = cityForCastData.filter((item: any) =>
        item.date.getHours() == 9
      );
    }
    catch {
      alert('Pls Try later...!');
    }
    finally {
      this.loaderVisibility = false;
    }
  }

}
