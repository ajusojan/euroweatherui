import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { WeatherService } from 'src/app/services';
import { CityDetailsComponent } from './city-details.component';

describe('CityDetailsComponent', () => {
  let component: CityDetailsComponent;
  let fixture: ComponentFixture<CityDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CityDetailsComponent],
      imports: [RouterTestingModule, HttpClientModule],
      providers: [WeatherService, {
        provide: ActivatedRoute,
        useValue: { snapshot: { paramMap: convertToParamMap({ 'id': '2759794' }) } }
      }]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CityDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('it should call weather service forecast', fakeAsync(() => {
    const spyForecast = spyOn((component as any).weatherService, 'getCityForCast');
    component.ngOnInit();
    tick();
    expect(spyForecast).toHaveBeenCalled();
  }));

  it('it should call weather service forecast', fakeAsync(() => {
    const spyForecast = spyOn((component as any).weatherService, 'getCityForCast').and.returnValue(of({ list: [{ dt: 123, dt_txt: '122', main: { temp: 1, sea_level: 11 } }] }));
    component.ngOnInit();
    tick();
    expect(spyForecast).toHaveBeenCalled();
  }));
});
