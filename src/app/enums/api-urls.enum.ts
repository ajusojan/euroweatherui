/**
 * API Urls
 */
export enum ApiUrls {
    CityListUrl = 'data/2.5/group',
    CityForcastUrl = 'data/2.5/forecast',
 }