import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { WeatherForCastResponse, WeatherResponse } from '../interfaces';
import { environment } from 'src/environments/environment';
import { ApiUrls } from '../enums/api-urls.enum';

@Injectable({
  providedIn: 'root'
})
/**
 * Weather Service Definition
 */
export class WeatherService {

  /** API baseurl */
  private baseUrl = environment.baseUrl;
  /** weather api token */
  private appid = environment.weatherToken;
  /** favourites city ids */
  private cityIds = '2759794,2643743,2800866,2968815,2993458';
  /**
   * Constructor method
   * @param euroWeatherHttp 
   */
  constructor(private euroWeatherHttp: HttpClient) { }

  /**
   * API call for getting favourite city data
   * @returns WeatherResponse
   */
  public getFavouriteCities(): Observable<WeatherResponse> {
    const url = `${this.baseUrl}/${ApiUrls.CityListUrl}?id=` + this.cityIds + '&units=metric&appid=' + this.appid
    return this.euroWeatherHttp.get(url) as Observable<WeatherResponse>;
  }

  /**
   * API call for getting forcast of selected city
   * @param cityId 
   * @returns WeatherForCastResponse
   */
  public getCityForCast(cityId: string): Observable<WeatherForCastResponse> {
    const url = `${this.baseUrl}/${ApiUrls.CityForcastUrl}?id=` + cityId + '&units=metric&appid=' + this.appid
    return this.euroWeatherHttp.get(url) as Observable<WeatherForCastResponse>;
  }
}
